import QtQuick 2.9
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import "js/wumpus.js" as Wumpus

MainView {
    id: mainView
    objectName: 'mainView'
    applicationName: 'wumpus.cibersheep'
    automaticOrientation: true

    signal newLine(string lineToPrint)
    property var nArrows: Wumpus.Wumpus.arrows
    property int nMoves: 0
    
    anchorToKeyboard: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        id: mainPage
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Hunt the Wumpus')
        }
        
        RowLayout {
        	id: info
        	width: parent.width
        	spacing: units.gu(2)
            anchors.top: header.bottom
            anchors.margins: units.gu(2)
        	
        	Label {
                width: parent.width / 2
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                text: i18n.tr('MOVEMENTS: %1').arg(nMoves)
                
                wrapMode: Text.Wrap
            }
            
            Label {
                width: parent.width / 2
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                text: i18n.tr('ARROWS:  %1').arg(nArrows)
                visible: Wumpus.Wumpus.arrows
                
                wrapMode: Text.Wrap
            }
        }
        
        ScrollView {
            anchors.left: parent.left
            anchors.right: parent.right   
            anchors.top: info.bottom
            anchors.bottom: inputArea.top
            anchors.bottomMargin: units.gu(2)
            contentItem: gameText
        }

        ListView {
            id: gameText
            width: parent.width
            height: parent.height
            anchors.leftMargin: units.gu(2)
            anchors.rightMargin: units.gu(2)
            model: linesOfText
            delegate: gameTextDelegate
            
            highlightFollowsCurrentItem: true
        }
        
        Row {
            id: inputArea
            spacing: units.gu(1)
            width: mainPage.width - instruction.width

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                margins: units.gu(2)
            }

            TextField {
                id: command
                width: parent.width - instruction.width

                onAccepted: {
                    command.focus = false;

                    linesOfText.append({
                        line: "\n?> " + command.text + "\n"
                    });
                    
                    gameText.incrementCurrentIndex();
                    Wumpus.Wumpus.input(command.text);
                    command.text = "";
                }

                onFocusChanged: {
                    if (!command.focus) {
                        command.focus = true
                    }
                }
            }

            Button {
                id: instruction
                width: units.gu(4)
                height: width
                enabled: command.text !== ""
                //color: darkColor
                opacity: enabled ? 1 : .4
                //Make sure the touch area is big enough
                sensingMargins.all: units.gu(2)

                Icon {
                    width: units.gu(2)
                    anchors.centerIn: parent
                    //color: instruction.enabled
                    //    ? lighterColor
                    //    : Theme.palette.disabled.foregroundText
                    name: "keyboard-enter"
                }

                onClicked: command.accepted()
            }   
        }
        
        ListModel {
            id: linesOfText
        }
        
        Component {
            id: gameTextDelegate
    
            Label {
                width: parent.width
                text: line
                
                wrapMode: Text.Wrap
            }
        }
    }
    
    Component.onCompleted: {
    	mainView.newLine("<br/><h1>" + i18n.tr("HUNT THE WUMPUS") + "</h1><br/>");
        command.focus = true;
        
        Wumpus.Wumpus.print = function(line) {
	        mainView.newLine(line)
        };
        
        //Code from https://stackoverflow.com/questions/9134686/adding-code-to-a-javascript-function-programmatically
        Wumpus.Wumpus.input = (function(input) {
        	//Redifined input function
			var cached_function = Wumpus.Wumpus.input;

			return function() {
				//Let's compare the number of arrows before and after the original input function
				var origArrowsCount = this.arrows;
				
				//Let's compare the hunter position
				var hunterPos = this.hunter;
				var result = cached_function.apply(this, arguments); // use .apply() to call it
		    	if (origArrowsCount !== this.arrows) {
		    		nArrows = Wumpus.Wumpus.arrows
	    		}
	    		
	    		if (hunterPos !== this.hunter) {
		    		++nMoves;
	    		}
	    		
	    		if (this.resetting) {
	    			nMoves = 0;
    			}

				return result;
			};
		})();	
        
        Wumpus.Wumpus.startup();
    }
    
    onNewLine: {
        linesOfText.append({
            line: lineToPrint
        });
        
        gameText.incrementCurrentIndex()
        //console.log("qml",lineToPrint);
        
        //console.log("\n\nARROWS: ", Wumpus.Wumpus.arrows)
    }
}
