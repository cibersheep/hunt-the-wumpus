# Hunt the Wumpus

Fire one your 'crooked arrows' through the caves to kill the Wumpus in this text-based adventure game developed by Gregory Yob in 1973

## Translations

Translations are welcome (now that they are enabled :D), use the app template under po/wumpus.cibersheep.pot and send your po file as a PR (or contact me)

## Building and testing

The app can run and be tested under any system that can run Docker and Python.
You need to:

- Install clickable [how to](https://clickable-ut.dev/en/latest/install.html)
- Clone this repo to your computer `git clone https://gitlab.com/cibersheep/hunt-the-wumpus`
  - Alternatively, [download](https://gitlab.com/cibersheep/hunt-the-wumpus/-/archive/master/hunt-the-wumpus-master.zip) the project and uncompress it in your computer
- Navigate to the directory containting the project and run `clickable desktop`

Please, inform of any bug you encounter.

Thank you

## License

### Ubuntu Touch app
Copyright (C) 2019  Joan CiberSheep

Licensed under the MIT license.

### Javascript port of the game
Copyright (C) 2009  Benjam Welker

### Original Hunt the Wumpus game
Copyright (C) 1973  Gregory Yob

### Original Hunt the Wumpus 2 game
Copyright (C) 1977  Gregory Yob
