# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the wumpus.cibersheep package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: wumpus.cibersheep\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-11-20 09:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:30 wumpus.desktop.in.h:1
msgid "Hunt the Wumpus"
msgstr ""

#: ../qml/Main.qml:43
msgid "MOVEMENTS: %1"
msgstr ""

#: ../qml/Main.qml:51
msgid "ARROWS:  %1"
msgstr ""

#: ../qml/Main.qml:154
msgid "HUNT THE WUMPUS"
msgstr ""

#: ../qml/js/wumpus.js:314
msgid "n"
msgstr ""

#: ../qml/js/wumpus.js:334
msgid "ERROR"
msgstr ""

#: ../qml/js/wumpus.js:349
msgid "y"
msgstr ""

#: ../qml/js/wumpus.js:364
msgid "NOT POSSIBLE -"
msgstr ""

#. TRANSLATORS this is m for "move"
#. TRANSLATORS: m for "move"
#: ../qml/js/wumpus.js:366 ../qml/js/wumpus.js:501
msgid "m"
msgstr ""

#: ../qml/js/wumpus.js:379
msgid "ZAP--SUPER BAT SNATCH! ELSEWHEREVILLE FOR YOU!"
msgstr ""

#: ../qml/js/wumpus.js:402 ../qml/js/wumpus.js:509
msgid "NO. OF ROOMS (1-5)"
msgstr ""

#: ../qml/js/wumpus.js:414
msgid "ARROWS AREN'T THAT CROOKED - TRY ANOTHER ROOM"
msgstr ""

#: ../qml/js/wumpus.js:426
msgid "ROOM #"
msgstr ""

#: ../qml/js/wumpus.js:451
msgid "OUCH! ARROW GOT YOU!"
msgstr ""

#: ../qml/js/wumpus.js:458
msgid "AHA! YOU GOT THE WUMPUS!"
msgstr ""

#: ../qml/js/wumpus.js:466
msgid "MISSED"
msgstr ""

#: ../qml/js/wumpus.js:470 ../qml/js/wumpus.js:627
msgid "TSK TSK TSK - WUMPUS GOT YOU!"
msgstr ""

#. TRANSLATORS: q for "quit"
#: ../qml/js/wumpus.js:495
msgid "q"
msgstr ""

#: ../qml/js/wumpus.js:503
msgid "WHERE TO"
msgstr ""

#. TRANSLATORS: s for "Shoot"
#: ../qml/js/wumpus.js:507
msgid "s"
msgstr ""

#: ../qml/js/wumpus.js:543
msgid "YOU ARE IN ROOM %1"
msgstr ""

#: ../qml/js/wumpus.js:544
msgid "TUNNELS LEAD TO %1"
msgstr ""

#: ../qml/js/wumpus.js:559
msgid "SHOOT OR MOVE (S-M)"
msgstr ""

#: ../qml/js/wumpus.js:574
msgid "I SMELL A WUMPUS"
msgstr ""

#: ../qml/js/wumpus.js:578
msgid "I FEEL A DRAFT"
msgstr ""

#: ../qml/js/wumpus.js:582
msgid "BATS NEARBY"
msgstr ""

#: ../qml/js/wumpus.js:611
msgid "INSTRUCTIONS (Y-N)"
msgstr ""

#: ../qml/js/wumpus.js:625
msgid "... OOPS! BUMPED A WUMPUS!"
msgstr ""

#: ../qml/js/wumpus.js:635
msgid "YYYYIIIIEEEE . . . FELL IN PIT"
msgstr ""

#: ../qml/js/wumpus.js:729
msgid "<h2>WELCOME TO 'HUNT THE WUMPUS'</h2>"
msgstr ""

#: ../qml/js/wumpus.js:730
msgid ""
"THE WUMPUS LIVES IN A CAVE OF 20 ROOMS. EACH ROOM                         "
"HAS 3 TUNNELS LEADING TO OTHER ROOMS. (LOOK AT A                         "
"DODECAHEDRON TO SEE HOW THIS WORKS)"
msgstr ""

#: ../qml/js/wumpus.js:734
msgid "<h3>HAZARDS:</h3>"
msgstr ""

#: ../qml/js/wumpus.js:735
msgid "- <b>BOTTOMLESS PITS</b>"
msgstr ""

#: ../qml/js/wumpus.js:736
msgid ""
"TWO ROOMS HAVE BOTTOMLESS PITS IN THEM.                         IF YOU GO "
"THERE, YOU FALL INTO THE PIT (& LOSE!)"
msgstr ""

#: ../qml/js/wumpus.js:739
msgid "- <b>SUPER BATS</b>"
msgstr ""

#: ../qml/js/wumpus.js:740
msgid ""
"TWO OTHER ROOMS HAVE SUPER BATS. IF YOU                         GO THERE, A "
"BAT GRABS YOU AND TAKES YOU TO SOME OTHER                         ROOM AT "
"RANDOM. (WHICH MAY BE TROUBLESOME)"
msgstr ""

#: ../qml/js/wumpus.js:744
msgid "<h3>WUMPUS:</h3>"
msgstr ""

#: ../qml/js/wumpus.js:745
msgid ""
"THE WUMPUS IS NOT BOTHERED BY HAZARDS (HE HAS SUCKER                         "
"FEET AND IS TOO BIG FOR A BAT TO LIFT).  USUALLY                         HE "
"IS ASLEEP.  TWO THINGS WAKE HIM UP: YOU SHOOTING AN                         "
"ARROW OR YOU ENTERING HIS ROOM."
msgstr ""

#: ../qml/js/wumpus.js:749
msgid ""
"IF THE WUMPUS WAKES, HE MOVES ONE ROOM (75%)                         OR "
"STAYS STILL (25%).  AFTER THAT, IF HE IS WHERE YOU                         "
"ARE, HE EATS YOU UP AND YOU LOSE!"
msgstr ""

#: ../qml/js/wumpus.js:752 ../qml/js/wumpus.js:774 ../qml/js/wumpus.js:792
#: ../qml/js/wumpus.js:809
msgid "HIT RETURN TO CONTINUE"
msgstr ""

#: ../qml/js/wumpus.js:757
msgid "<h3>YOU:</h3>"
msgstr ""

#: ../qml/js/wumpus.js:758
msgid "EACH TURN YOU MAY MOVE OR SHOOT A CROOKED ARROW"
msgstr ""

#: ../qml/js/wumpus.js:759
msgid "- <b>MOVING:</b> YOU CAN MOVE ONE ROOM (THRU ONE TUNNEL)"
msgstr ""

#: ../qml/js/wumpus.js:760
msgid ""
"- <b>ARROWS:</b> YOU HAVE 5 ARROWS. YOU LOSE WHEN YOU RUN "
"OUT.                            EACH ARROW CAN GO FROM 1 TO 5 ROOMS. YOU AIM "
"BY SPECIFYING                            THE ROOMS YOU WANT THE ARROW TO GO "
"TO."
msgstr ""

#: ../qml/js/wumpus.js:763
msgid ""
"IF THE ARROW CAN'T GO THAT WAY (IF NO TUNNEL) IT "
"MOVES                            AT RANDOM TO THE NEXT ROOM."
msgstr ""

#: ../qml/js/wumpus.js:765
msgid "IF THE ARROW HITS THE WUMPUS, YOU WIN."
msgstr ""

#: ../qml/js/wumpus.js:766
msgid "IF THE ARROW HITS YOU, YOU LOSE."
msgstr ""

#: ../qml/js/wumpus.js:768
msgid "<h3>WARNINGS:</h3>"
msgstr ""

#: ../qml/js/wumpus.js:769
msgid ""
"WHEN YOU ARE ONE ROOM AWAY FROM A HAZARD,                            YOU'LL "
"GET:"
msgstr ""

#: ../qml/js/wumpus.js:771
msgid "- <b>WUMPUS:</b> 'I SMELL A WUMPUS'"
msgstr ""

#: ../qml/js/wumpus.js:772
msgid "- <b>BAT:</b> 'BATS NEARBY'"
msgstr ""

#: ../qml/js/wumpus.js:773
msgid "- <b>PIT:</b> 'I FEEL A DRAFT'"
msgstr ""

#: ../qml/js/wumpus.js:779
msgid "<h1>WELCOME TO WUMPUS II</h1>"
msgstr ""

#: ../qml/js/wumpus.js:780
msgid ""
"THIS VERSION HAS THE SAME RULES AS 'HUNT THE "
"WUMPUS'.                            HOWEVER, YOU NOW HAVE A CHOICE OF CAVES "
"TO PLAY IN.                            SOME CAVE ARE EASIER THAN OTHERS. ALL "
"CAVES HAVE 20                            ROOMS AND 3 TUNNELS LEADING FROM "
"ONE ROOM TO OTHER ROOMS."
msgstr ""

#: ../qml/js/wumpus.js:784
msgid "<b>0 - DODECAHEDRON</b>"
msgstr ""

#: ../qml/js/wumpus.js:785
msgid ""
"THE ROOMS OF THIS CAVE ARE ON A                            12-SIDED OBJECT, "
"EACH FORMS A PENTAGON.                            THE ROOMS ARE AT THE "
"CORNERS OF THE PENTAGONS.                            EACH ROOM HAVING "
"TUNNELS THAT LEAD TO 3 OTHER ROOMS."
msgstr ""

#: ../qml/js/wumpus.js:789
msgid "<b>1 - MOBIUS STRIP</b>"
msgstr ""

#: ../qml/js/wumpus.js:789
msgid ""
"THIS CAVE IS TWO ROOMS                            WIDE AND 10 ROOMS AROUND "
"(LIKE A BELT).                            YOU WILL NOTICE THERE IS A HALF "
"TWIST SOMEWHERE."
msgstr ""

#: ../qml/js/wumpus.js:797
msgid "<b>2 - STRING OF BEADS</b>"
msgstr ""

#: ../qml/js/wumpus.js:798
msgid ""
"FIVE BEADS IN A CIRCLE.                            EACH BEAD IS A DIAMOND "
"WITH A VERTICAL                            CROSS-BAR. THE RIGHT & LEFT "
"CORNERS LEAD                            TO NEIGHBORING BEADS. (THIS ONE IS "
"DIFFICULT TO PLAY)"
msgstr ""

#: ../qml/js/wumpus.js:802
msgid "<b>3 - HEX NETWORK</b>"
msgstr ""

#: ../qml/js/wumpus.js:803
msgid ""
"IMAGINE A HEX TILE FLOOR. TAKE                            A RECTANGLE WITH "
"20 POINTS (INTERSECTIONS)                            INSIDE (4X4). JOIN "
"RIGHT & LEFT SIDES TO MAKE A                            CYLINDER. THEN JOIN "
"TOP & BOTTOM TO FORM A                            TORUS (DOUGHNUT)."
msgstr ""

#: ../qml/js/wumpus.js:808
msgid "HAVE FUN IMAGINING THIS ONE!!"
msgstr ""

#: ../qml/js/wumpus.js:814
msgid ""
"CAVES 1-3 ARE REGULAR IN A SENSE THAT EACH ROOM                            "
"GOES TO THREE OTHER ROOMS & TUNNELS ALLOW TWO-WAY "
"TRAFFIC.                            HERE ARE SOME 'IRREGULAR' CAVES:"
msgstr ""

#: ../qml/js/wumpus.js:817
msgid "<b>4 - DENDRITE WITH DEGENRACIES</b>"
msgstr ""

#: ../qml/js/wumpus.js:818
msgid ""
"PULL A PLANT FROM                           THE GROUND. THE ROOTS & BRANCHES "
"FORM A                           DENDRITE - IE., THERE ARE NO LOOPING "
"PATHS                           DEGENERACY MEANS A) SOME ROOMS CONNECT "
"TO                           THEMSELVES AND B) SOME ROOMS HAVE MORE THAN "
"ONE                           TUNNEL TO THE SAME OTHER ROOM IE., 12 "
"HAS                           TWO TUNNELS TO 13."
msgstr ""

#: ../qml/js/wumpus.js:825
msgid "<b>5 - ONE WAY LATTICE</b>"
msgstr ""

#: ../qml/js/wumpus.js:826
msgid ""
"HERE ALL TUNNELS GO ONE                           WAY ONLY. TO RETURN, YOU "
"MUST GO AROUND THE CAVE (ABOUT 5 MOVES)."
msgstr ""

#: ../qml/js/wumpus.js:834
msgid "HAPPY HUNTING!"
msgstr ""

#: ../qml/js/wumpus.js:848
msgid "CAVE #(0-5)"
msgstr ""

#: ../qml/js/wumpus.js:860
msgid "HEE HEE HEE - THE WUMPUS'LL GET YOU NEXT TIME!!"
msgstr ""

#: ../qml/js/wumpus.js:862 ../qml/js/wumpus.js:876
msgid "SAME SETUP (Y-N)"
msgstr ""

#: ../qml/js/wumpus.js:874
msgid "HA HA HA - YOU LOSE!"
msgstr ""

#: wumpus.desktop.in.h:2
msgid "game;computer history;text games;classic games;solo game;"
msgstr ""
